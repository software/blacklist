#!/bin/bash
# Copyright (C) 2014  Michał Masłowski  <mtjm@mtjm.eu>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Verify the blacklist entries are correctly formatted.

bad_entries="$(egrep -v '^[^:]*:[^:]*:(sv|debian|hyperbola|parabola|fsf|fedora)?:[^:]*:.*$' *.txt)"

if [[ ! -z "$bad_entries" ]]; then
    printf "Incorrectly formatted entries:\n\n%s\n" "$bad_entries" >&2
    exit 1
fi

unsourced="$(egrep '^[^:]*:[^:]*::[^:]*:.*$' *.txt)"

if [[ ! -z "$unsourced" ]]; then
    printf "[citation needed]:\n\n%s\n" "$unsourced" >&2
    exit 1
fi
