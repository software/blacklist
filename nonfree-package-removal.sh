#!/bin/bash

# nonreplacement nonfree packages list
echo $(< blacklist.txt sed -e '/^#/d' -e 's/^[^:]*$/&::/' -e 's/^[^:]*:[^:]*$/&:/' | cut -d: -f1,2 | sed -n 's/:$//p' | sort -u) > blacklist_1.txt

# nonfree packages list
echo $(< blacklist.txt sed -e '/^#/d' -e 's/^[^:]*$/&::/' -e 's/^[^:]*:[^:]*$/&:/' | cut -d: -f1 | sort -u) > blacklist_2.txt
